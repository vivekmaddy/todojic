from django.test import TestCase
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from rest_framework.test import APITestCase
from rest_framework import status

from .models import *


# Create your tests here.


class TodoModelTest(TestCase):
    def setUp(self):
        user_payload = {
            "username": "vivek.madhavan",
            "email": "vivek.madhavan1996@gmail.com",
            "first_name": "vivek",
            "last_name": "madhavan",
            "password": "123"
        }
        self.user = User.objects.create_user(
            **user_payload
        )

        self.data = {
            "name": "test todo",
            "due_dt": "2023-11-09T10:10:59",
            "description": "todo description",
            "user_id": self.user.id,
        }
        self.todo_data = TodoLists.objects.create(
            **self.data
        )

    def test_todo_model_data(self):
        data = self.data.copy()
        data["priority"] = 1
        data["status"] = 1
        data["remind_me"] = True
        data["is_notified"] = False
        new_data = model_to_dict(self.todo_data)
        new_data.pop('async_task_id')
        new_data.pop('id')
        new_data["user_id"] = new_data.pop('user')
        self.assertEqual(data, new_data)


class TodoViewsetTest(APITestCase):
    fixtures = ['dumpdata.json']

    def setUp(self):
        login_payload = {
            "username": "vivek.madhavan",
            "password": "123"
        }
        response = self.client.post(f'/users/auth/login', login_payload, format='json')
        data = response.json()
        access_token = data["data"]["access"]
        self.headers = {
            'HTTP_AUTHORIZATION': f'Bearer {access_token}'
        }

        user_id = data["data"]["user"]["id"]
        sample_data = {
            "name": "test todo",
            "due_dt": "2023-11-09T20:42:59",
            "description": "reminder",
            "priority": 1,
            "remind_me": True,
            "status": 1,
            "user_id": user_id
        }
        self.test_data = TodoLists.objects.create(**sample_data)

    def test_todo_list(self):
        response = self.client.get(f'/todo/manage_todo?page=1&limit=5', **self.headers)
        data = response.json()
        data = data["data"]
        result_count = len(data["results"])
        with self.subTest("TODO LIST STATUS CODE"):
            self.assertEqual(200, response.status_code)
        with self.subTest("TODO LIST PAGINATION COUNT"):
            self.assertEqual(5, result_count)

    def test_todo_create(self):
        data = {
            "name": "test todo",
            "due_dt": "2023-11-09T20:42:59",
            "description": "reminder",
            "priority": 1,
            "remind_me": True,
            "status": 1
        }
        response = self.client.post(f'/todo/manage_todo', data, **self.headers, format='json')
        data = response.json()
        self.assertEqual(200, response.status_code)

    def test_todo_retrieve(self):
        response = self.client.get(f'/todo/manage_todo/{self.test_data.id}', **self.headers)
        self.assertEqual(200, response.status_code)

    def test_todo_patch(self):
        data = {
            "name": "test todo updated",
        }
        response = self.client.patch(f'/todo/manage_todo/{self.test_data.id}', data, **self.headers, format='json')
        data = response.json()
        with self.subTest("TEST TODO PATCH VIEW STATUS CODE"):
            self.assertEqual(200, response.status_code)
        with self.subTest("TEST TODO PATCH VIEW DATA CHECK"):
            self.assertEqual("test todo updated", data["data"]["name"])

    def test_todo_destroy(self):
        response = self.client.delete(f'/todo/manage_todo/{self.test_data.id}', **self.headers)
        with self.subTest("TEST TODO DESTROY VIEW STATUS CODE"):
            self.assertEqual(200, response.status_code)
        with self.subTest("TEST TODO DESTROY VIEW DATA CHECK"):
            self.assertFalse(TodoLists.objects.filter(id=self.test_data.id).exists())

    def test_todo_get_filter(self):
        response = self.client.get(f'/todo/manage_todo/get_filter', **self.headers)
        self.assertEqual(200, response.status_code)
