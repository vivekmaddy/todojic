import json
from datetime import datetime
from pytz import timezone

from rest_framework import mixins, viewsets
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination

from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.conf import settings

from celery.result import AsyncResult
from core.celery import app

from .serializers import TodoListsSerializer, TodoListCreateSerializer
from .models import TodoLists
from .tasks import delayed_send_email, due_dt_reminder_email


class TodoViewset(viewsets.ModelViewSet):
    queryset = TodoLists.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = TodoListsSerializer
    pagination_class = PageNumberPagination

    def create_due_dt_reminder_task(self, payload, instance):
        tz = timezone(settings.TIME_ZONE)
        eta = datetime.strptime(payload["due_dt"], "%Y-%m-%dT%H:%M:%S").replace(second=0)
        eta = tz.localize(eta)
        result = due_dt_reminder_email.apply_async(eta=eta, args=(instance.id,))
        if result:
            TodoLists.objects.filter(id=instance.id).update(async_task_id=result.id)

    def get_queryset(self):
        params = self.request.GET
        status = params.get('status', None)
        priority = params.get('priority', None)
        due_dt_range = params.get('due_dt_range', None)
        sort_order = params.get('sort', None)
        filter_dict = {}
        if status:
            filter_dict["status__in"] = json.loads(status)
        if priority:
            filter_dict["priority__in"] = json.loads(priority)
        if due_dt_range:
            due_dt_l = json.loads(due_dt_range)
            if len(due_dt_l) == 2:
                filter_dict["due_dt__date__range"] = due_dt_l
            else:
                filter_dict["due_dt__date"] = due_dt_l[0]

        qs = self.queryset.filter(user=self.request.user, **filter_dict).order_by("-id")
        if sort_order:
            qs = qs.order_by(sort_order)
        return qs

    def create(self, request, *args, **kwargs):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            payload = request.data.copy()
            payload["user"] = request.user.id
            serializer = TodoListCreateSerializer(data=payload)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            response["data"] = self.get_serializer(serializer.instance).data
            response["message"] = "Created"

            if payload.get("due_dt", False):
                self.create_due_dt_reminder_task(payload, serializer.instance)

        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)

    def list(self, request, *args, **kwargs):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            qs = self.get_queryset()
            paginator = self.pagination_class()
            paginator.page_size = request.GET.get("limit", 5)
            result_page = paginator.paginate_queryset(qs, request)
            result = self.get_serializer(result_page, many=True)
            response["data"] = paginator.get_paginated_response(result.data).data
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)

    def retrieve(self, request, pk=None):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            qs = self.get_object()
            data = self.get_serializer(qs).data
            response["data"] = data
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)

    def partial_update(self, request, pk=None):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            qs = self.get_object()
            old_due_dt, old_status = qs.due_dt, qs.status
            qs_dict = model_to_dict(qs)
            serializer = TodoListCreateSerializer(qs, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            response["data"] = self.get_serializer(serializer.instance).data
            response["message"] = "Updated"

            if old_due_dt != serializer.instance.due_dt:
                existing_task = qs.async_task_id
                TodoLists.objects.filter(id=qs.id).update(is_notified=False)
                if existing_task:
                    AsyncResult(existing_task).revoke(terminate=True)
                    self.create_due_dt_reminder_task(request.data, serializer.instance)

            if old_status != serializer.instance.status:
                result = delayed_send_email.delay(payload=request.data, data=qs_dict)
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)

    def destroy(self, request, pk=None):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            qs = self.get_object()
            qs.delete()
            response["message"] = "Deleted!"
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)

    @action(detail=False, methods=['GET'])
    def get_filter(self, request):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            response["data"] = {
                "all_priority": [{"id": i[0], "value": i[1]} for i in TodoLists.priority_choices],
                "all_status": [{"id": i[0], "value": i[1]} for i in TodoLists.status_choices],
            }
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)


