from django.urls import path, include
from rest_framework.routers import DefaultRouter

from todo import views

router = DefaultRouter(trailing_slash=False)
router.register("manage_todo", views.TodoViewset, basename="manage_todo")

urlpatterns = [
    path('', include(router.urls))
]
