from celery import shared_task
from common.utils import send_email_notifications


@shared_task(name='OTP EMAIL')
def delayed_send_email(email, otp):
    template = "otp.html"
    context = {
        "to": email,
        "subject": "TodoApp OTP",
        "email": email,
        "otp": otp,
    }
    result = send_email_notifications(template, context)
    print("OTP EMAIL result", result)
