from django.core.mail import send_mail
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import get_template


def send_email_notifications(template, context):
    try:
        template = get_template(template)
        content = template.render(context)
        msg = EmailMessage(context["subject"], content, settings.EMAIL_HOST_USER, to=[context["to"], ])
        msg.content_subtype = "html"
        delivered = msg.send()
        return "delivered" if delivered else "failed"
    except Exception as err:
        return str(err)
