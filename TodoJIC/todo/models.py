from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TodoLists(Base):
    status_choices = (
        (1, "todo"),
        (2, "in-progress"),
        (3, "completed"),
        (4, "cancelled"),
    )
    priority_choices = (
        (1, "low"),
        (2, "medium"),
        (3, "high"),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    due_dt = models.DateTimeField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    priority = models.IntegerField(choices=priority_choices, default=1)
    remind_me = models.BooleanField(default=True)
    status = models.IntegerField(choices=status_choices, default=1)
    is_notified = models.BooleanField(default=False)
    async_task_id = models.TextField(blank=True, null=True)  # due date reminder celery taskid
