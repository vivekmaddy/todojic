import random

from rest_framework import mixins, viewsets
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from rest_framework_simplejwt.tokens import RefreshToken

from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from .serializers import SignupSerializer, UserDetailsSerializer
from .models import UserOTP
from .tasks import delayed_send_email

from common.utils import send_email_notifications


# Create your views here.
def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


class SignupViewset(mixins.CreateModelMixin,
                    viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = SignupSerializer
    permission_classes = []


    def create(self, request, *args, **kwargs):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            payload = request.data
            password, repassword, otp, email = payload["password"], payload.pop("repassword"), payload.pop("otp"), \
                payload["email"]

            if password != repassword:
                raise Exception("Both passwords are not same!")
            if User.objects.filter(username=payload["username"]).exists():
                raise Exception("Username already taken!")
            if not UserOTP.objects.filter(email=email, otp=otp).exists():
                raise Exception("Invalid OTP!")

            serializer = self.get_serializer(data=payload)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            response["message"] = "Signup success"
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)

    @action(detail=False, methods=['POST'])
    def send_resend_otp(self, request):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            payload = request.data
            email = payload["email"]
            otp = random.randint(1001, 9999)
            lookup_params = {
                'email': email,
            }
            update_params = {
                'email': email,
                'otp': otp
            }
            obj, created = UserOTP.objects.update_or_create(defaults=update_params, **lookup_params)
            result = delayed_send_email.delay(email, otp)

            if not result:
                raise Exception("OTP sending via email failed!")

            response["message"] = "OTP generated, please check your mail!"
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)


class LoginViewset(mixins.CreateModelMixin,
                   viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserDetailsSerializer
    permission_classes = []

    def create(self, request, *args, **kwargs):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            payload = request.data
            username, password = payload["username"], payload["password"]
            user = authenticate(username=username, password=password)

            if not user:
                raise Exception("Incorrect username or password")

            response["data"]["user"] = self.get_serializer(user).data
            tokens = get_tokens_for_user(user)

            if len(tokens.values()) != 2:
                raise Exception("Login failed!")

            response["data"].update(tokens)
            response["message"] = "Login success"
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)


class UserDetailsViewset(mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserDetailsSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(id=self.request.user.id)

    def list(self, request):
        response = {
            "status": HTTP_200_OK,
            "message": "Ok",
            "data": {}
        }
        status_code = HTTP_200_OK
        try:
            instance = self.get_queryset()
            response["data"] = self.get_serializer(instance, many=True).data
        except Exception as err:
            response["message"] = str(err)
            response["status"] = status_code = HTTP_400_BAD_REQUEST
        return Response(response, status=status_code)