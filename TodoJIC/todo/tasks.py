from datetime import datetime

from celery import shared_task
from common.utils import send_email_notifications

from .models import TodoLists


@shared_task(name='TODO STATUS UPDATE EMAIL')
def delayed_send_email(payload, data):
    updated_instance = TodoLists.objects.get(id=data["id"])
    if 'status' in payload and data["status"] != payload["status"]:

        template = "status_email_template.html"
        context = {
            "to": updated_instance.user.email,
            "subject": "TodoApp : task status update",
            "username": updated_instance.user.username,
            "status": updated_instance.get_status_display(),
            "name": updated_instance.name,
        }
        result = send_email_notifications(template, context)
        print("result", result)
        return "TODO EMAIL delivered"


@shared_task(name='TODO DUE DATE REMINDER EMAIL')
def due_dt_reminder_email(instance_id):
    now = datetime.now()
    datas = TodoLists.objects.filter(id=instance_id, due_dt__date__lte=now, remind_me=True, is_notified=False).exclude(
        status=3)
    data = datas.first()
    template = "todo_reminder.html"
    context = {
        "to": data.user.email,
        "subject": "TodoApp : task is about to end",
        "username": data.user.username,
        "task_name": data.name,
        "due_dt": data.due_dt.strftime("%d/%m%Y %I:%M%p"),
        "status": data.get_status_display(),
    }
    TodoLists.objects.filter(id=instance_id).update(is_notified=True)
    result = send_email_notifications(template, context)
    print("result", result)
