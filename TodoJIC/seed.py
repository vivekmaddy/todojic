from django_seed import Seed
from todo.models import TodoLists
from django.contrib.auth.models import User

seeder = Seed.seeder()

# Define a user with id 1
user = User.objects.get(username="vivek.madhavan")

seeder.add_entity(TodoLists, 20, {
    'user': user,
    'name': seeder.faker.name(),
    'due_dt': seeder.faker.date_time_between(start_date='-7d', end_date='+7d'),
    'description': seeder.faker.text(max_nb_chars=200),
    'priority': seeder.faker.random_element(elements=[1, 2, 3]),
    'remind_me': seeder.faker.boolean(chance_of_getting_true=80),
    'status': seeder.faker.random_element(elements=[1, 2, 3, 4]),
    'is_notified': seeder.faker.boolean(chance_of_getting_true=20),
})

# Execute the seed to insert the data
inserted_pks = seeder.execute()