from django.contrib import admin
from .models import TodoLists


# Register your models here.


@admin.register(TodoLists)
class AdminTodoLists(admin.ModelAdmin):
    list_display = ("id", "user", "name", "due_dt", "is_notified", "async_task_id")
