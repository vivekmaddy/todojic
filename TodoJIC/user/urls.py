from django.urls import path, include
from rest_framework.routers import DefaultRouter

from user import views

user_auth_router = DefaultRouter(trailing_slash=False)
user_auth_router.register("signup", views.SignupViewset, basename="signup")
user_auth_router.register("login", views.LoginViewset, basename="login")

user_router = DefaultRouter(trailing_slash=False)
user_router.register("get_details", views.UserDetailsViewset, basename="get_details")


urlpatterns = [
    path('auth/', include(user_auth_router.urls)),
    path('', include(user_router.urls))
]
