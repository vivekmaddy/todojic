from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status

import random
from .models import *


# Create your tests here.


class TodoModelDataTest(TestCase):
    def setUp(self):
        self.user_payload = {
            "username": "sampleuser",
            "email": "sampleuser@gmail.com",
            "first_name": "fname",
            "last_name": "lname",
            "password": "123"
        }
        self.otp_payload = {
            "email": "sampletest@gmail.com",
            "otp": random.randint(1000, 9999)
        }
        self.user = User.objects.create_user(
            **self.user_payload
        )
        self.user_otp = UserOTP.objects.create(
            **self.otp_payload
        )

    def model_test_create_test(self):
        with self.subTest('MODELTEST : AUTHUSER CREATE'):
            created_data = {
                "username": self.user.username,
                "email": self.user.email,
                "first_name": self.user.first_name,
                "last_name": self.user.last_name,
            }
            self.user_payload.pop("password")
            self.assertEqual(created_data, self.user_payload)

        with self.subTest('MODELTEST : USEROTP CREATE'):
            otp_payload = {
                "email": self.user_otp.email,
                "otp": self.user_otp.otp
            }
            self.assertEqual(otp_payload, self.otp_payload)


class UserSignupAPITest(APITestCase):
    def test_user_signup_view(self):
        otp_payload = {
            "email": "vivek.madhavan1996@gmail.com"
        }
        response = self.client.post(f'/users/auth/signup/send_resend_otp', otp_payload)
        with self.subTest('USERSIGNUP OTP API : POST REQ'):
            self.assertEqual(response.status_code, 200)
        otp = UserOTP.objects.get(email=otp_payload["email"]).otp
        signup_payload = {
            "username": "vivek.madhavan",
            "email": "vivek.madhavan1996@gmail.com",
            "first_name": "vivek",
            "last_name": "madhavan",
            "password": "123",
            "repassword": "123",
            "otp": 123
        }

        response = self.client.post(f'/users/auth/signup', signup_payload, format='json')
        data = response.json()
        with self.subTest("USER SIGNUP TEST INVALID OTP: POST REQ"):
            self.assertEqual(400, data['status'])

        signup_payload_copy = signup_payload.copy()
        signup_payload_copy["otp"] = otp
        response_ = self.client.post(f'/users/auth/signup', signup_payload_copy, format='json')
        data = response_.json()
        with self.subTest("USER SIGNUP SUCCESS TEST : POST REQ"):
            self.assertEqual(200, data['status'])


class UserLoginTest(APITestCase):
    def setUp(self):
        data = {
            "username": "vivek.madhavan",
            "email": "vivek.madhavan1996@gmail.com",
            "first_name": "vivek",
            "last_name": "madhavan",
            "password": "123"
        }
        self.user = User.objects.create_user(**data)

    def test_user_login_test(self):
        login_payload = {
            "username": "vivek.madhavan",
            "password": "123"
        }
        response = self.client.post(f'/users/auth/login', login_payload, format='json')
        data = response.json()
        with self.subTest("USER LOGIN STATUSCODE"):
            self.assertEqual(200, response.status_code)
        with self.subTest("USER LOGIN ACCESS TOKEN CHECK"):
            self.assertTrue('access' in data["data"])
        with self.subTest("USER LOGIN ACCESS TOKEN CHECK"):
            self.assertTrue('refresh' in data["data"])

    def test_invalid_credential(self):
        login_payload = {
            "username": "vivek.madhavan",
            "password": "invalidpass"
        }
        response = self.client.post(f'/users/auth/login', login_payload, format='json')
        self.assertEqual(400, response.status_code)

    def test_authentication_fail(self):
        headers = {}
        response = self.client.get(f'/todo/manage_todo', **headers)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class UserDetailsTest(APITestCase):
    def setUp(self):
        data = {
            "username": "vivek.madhavan",
            "email": "vivek.madhavan1996@gmail.com",
            "first_name": "vivek",
            "last_name": "madhavan",
            "password": "123"
        }
        self.user = User.objects.create_user(**data)

    def test_userdetails_view(self):
        login_payload = {
            "username": "vivek.madhavan",
            "password": "123"
        }
        response = self.client.post(f'/users/auth/login', login_payload, format='json')
        data = response.json()
        access_token = data["data"]["access"]
        headers = {
            'HTTP_AUTHORIZATION': f'Bearer {access_token}'
        }
        response = self.client.get(f'/users/get_details', **headers)
        self.assertEqual(200, response.status_code)
