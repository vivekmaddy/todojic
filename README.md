# TodoJIC

## Introduction
You are tasked with creating a Todo project using Django and Django Rest Framework (DRF). The
project should include user authentication using JSON Web Tokens (JWT) and should send email
notifications asynchronously whenever the status of a Todo item changes. Follow best practices
throughout the development process.


## About Project

* Project Flow
    - first user need to enter `emailid` for recieving `OTP` to mail id. send email is done by using celery delay() method
    - user can signup to todoapp by using the otp and filling other signup details
    - After signup user can login. `access & refresh token` will be generated. simple-jwt used for generating JWT tokens
    <br><br>
    - Todo CRUD operation can be done by passing `token in the header` and with required fields
    - Todo apis accepts `POST, GET, GET with id, PATCH, DELETE` http methods
        - `POST` on create() a celery task will be scheduled based on the duedate. this celery task will execute only if the `reminder is True`, this task will run on the due date for the reminder notification.
        - `PATCH` on partial_update() will create a celery task only if ther is `any change in due date`. <br>
           If the any change in due date and if the todo task have a scheduled celery task that will be revoked and a new celery task will be created. 


* Folders and Files <br>
    `core` : is the main project <br>
    `user` : is an app, eveything related to user defined inside user (Signup, Login etc)<br>
    `todo` : is an app, eveything related to todo defined inside todo (crud etc..)
    `common` : common methods are defined inside `common\utlis.py` <br>
    `todo/tasks.py` & `user/tasks.py`: celery related tasks are defined inside tasks.py (in both todo & user).
     celery tasks are used for email sending (for status change notifications, due date reminder notifications and for sending otp and )
    `dumpdata.json` : dump data using for unit test<br>
    `seed.py` : used for creating test data<br>

* Endpoints <br>
    Please visit for postman collection `https://www.postman.com/research-astronomer-51289061/workspace/passwordmanager/request/17887162-0165f270-e089-45f3-94b2-c8a31e94a402`

    * For Signup
        - `{{localhost}}/users/auth/signup/send_resend_otp` pass email as payload this will generate OTP and send it to you via email
            ```
            METHOD : POST
            payload :
            {
                "email" : "vivek.madhavan1996@gmail.com"
            }
            response :
            {
                "status": 200,
                "message": "OTP generated, please check your mail!",
                "data": {}
            }
            ```
        - `{{localhost}}/users/auth/signup` pass the otp with other data for signup
            ```
            METHOD : POST
            payload :
            {
                "username" : "vivek.madhavan",
                "email" : "vivek.madhavan1996@gmail.com",
                "first_name" : "vivek",
                "last_name" : "madhavan",
                "password" : "123",
                "repassword" : "123",
                "otp" : 7636
            }
            response :
            {
                "status": 200,
                "message": "Signup success",
                "data": {}
            }
            ```
    * For Login
        - `{{localhost}}/users/auth/login` 
            ```
            METHOD : POST
            payload :
            {
                "username" : "vivek.madhavan",
                "password" : "123"
            }
            response :
            {
                "status": 200,
                "message": "Login success",
                "data": {
                    "user": {
                        "id": 11,
                        "username": "vivek.madhavan",
                        "full_name": "vivek madhavan",
                        "email": "vivek.madhavan1996@gmail.com"
                    },
                    "refresh": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY5OTYzODM0NiwiaWF0IjoxNjk5NTUxOTQ2LCJqdGkiOiJmZDdmNThhMDAxOTg0YzQ2YjVmMTdmMTNlNWU4YTFkMiIsInVzZXJfaWQiOjExfQ.7r4SFfLzxB7kYWWig4FBODAnJwjJm8W8jDH-4OMo63g",
                    "access": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjk5NjM4MzQ2LCJpYXQiOjE2OTk1NTE5NDYsImp0aSI6IjYxMmIwMmI1OWY3NjQzMGNhYTM5ZGU3ZjRkYjIyZWZjIiwidXNlcl9pZCI6MTF9.ywReFtHphz2P08gGVjvYkEPbAXyZ9sWs2S3ej_u667I"
                }
            }
            ```



    * For logged user details
        - `http://127.0.0.1:8000/users/get_details` token required
        ```
        METHOD : GET
        response:
        {
            "status": 200,
            "message": "Ok",
            "data": [
                {
                    "id": 11,
                    "username": "vivek.madhavan",
                    "full_name": "vivek madhavan",
                    "email": "vivek.madhavan1996@gmail.com"
                }
            ]
        }
        ```



    * For Todo CRUD (access token required for below endpoints, pass token in header Authorization `Bearer token`)
        - `http://127.0.0.1:8000/todo/manage_todo` for create todo
            ```
            METHOD : POST
            payload :
            {
                "name" : "f",
                "due_dt" : "2023-11-09T20:42:59",  // not mandatory
                "description" : "reminder",     // not mandatory
                "priority" : 1,     // not mandatory
                "remind_me" : true, // not mandatory
                "status" : 1    // not mandatory
            }
            response :
            {
                "status": 200,
                "message": "Created",
                "data": {
                    "id": 66,
                    "status": "todo",
                    "priority": "low",
                    "created_at": "2023-11-09T23:17:24.644249+05:30",
                    "updated_at": "2023-11-09T23:17:24.644292+05:30",
                    "name": "f",
                    "due_dt": "2023-11-09T20:42:59+05:30",
                    "description": "reminder",
                    "remind_me": true,
                    "is_notified": false,
                    "async_task_id": null,
                    "user": 11
                }
            }
            ```
        - `http://127.0.0.1:8000/todo/manage_todo?page=1&priority=[1,2]&status=[2, 3]&limit=4&due_dt_range=["2023-11-08", "2023-11-13"]&sort=-due_dt`
            * for sorting pass sort=value, value will be same as key.
            * for filtering use 
                - priority=[1,2]
                - status=[2, 3]
                - due_dt_range= [from , to]  // if u send only one date it will be filtered by that date
                - limit=value  // for pagination
            ```
            METHOD : GET
            response : 
            {
                "status": 200,
                "message": "Ok",
                "data": {
                    "count": 40,
                    "next": "http://127.0.0.1:8000/todo/manage_todo?limit=1&page=2&sort=-due_dt",
                    "previous": null,
                    "results": [
                        {
                            "id": 107,
                            "status": "todo",
                            "priority": "medium",
                            "created_at": "1985-08-17T14:55:28+05:30",
                            "updated_at": "1997-09-09T16:59:53+05:30",
                            "name": "Jonathan Thomas",
                            "due_dt": "2023-11-10T15:18:34+05:30",
                            "description": "Why identify knowledge believe community deal heavy describe.\nOff key seek course. Student weight effect first per.",
                            "remind_me": true,
                            "is_notified": false,
                            "async_task_id": null,
                            "user": 11
                        }
                    ]
                }
            }
            ```
        - `http://127.0.0.1:8000/todo/manage_todo/19` to get more details about a task. pass the id of task
            ```
            METHOD : GET
            response:
            {
                "status": 200,
                "message": "Ok",
                "data": {
                    "id": 107,
                    "status": "todo",
                    "priority": "medium",
                    "created_at": "1985-08-17T14:55:28+05:30",
                    "updated_at": "1997-09-09T16:59:53+05:30",
                    "name": "Jonathan Thomas",
                    "due_dt": "2023-11-10T15:18:34+05:30",
                    "description": "Why identify knowledge believe community deal heavy describe.\nOff key seek course. Student weight effect first per.",
                    "remind_me": true,
                    "is_notified": false,
                    "async_task_id": null,
                    "user": 11
                }
            }
            ```
        - `http://127.0.0.1:8000/todo/manage_todo/65` to update. pass keys based on updating or all keys are also accepted
        ```
        METHOD : PATCH
        payload : 
        {
            "due_dt" : "2023-11-09T20:44:59"
        }
        response:
        {
            "status": 200,
            "message": "Updated",
            "data": {
                "id": 65,
                "status": "todo",
                "priority": "low",
                "created_at": "2023-11-09T20:40:19.884296+05:30",
                "updated_at": "2023-11-09T20:40:50.474785+05:30",
                "name": "f",
                "due_dt": "2023-11-09T20:44:59+05:30",
                "description": "reminder",
                "remind_me": true,
                "is_notified": false,
                "async_task_id": "b9bd5ef9-0765-4dc1-988f-877cca772a0f",
                "user": 10
            }
        }
        ```
        - `http://127.0.0.1:8000/todo/manage_todo/14` to delete a task
        ```
        METHOD : DELETE
        response:
        {
            "status": 200,
            "message": "Deleted!",
            "data": {}
        }
        ```
    
    * For filter data
        - `http://127.0.0.1:8000/todo/manage_todo/get_filter` token required
        ```
        METHOD : GET
        response:
        {
            "status": 200,
            "message": "Ok",
            "data": {
                "all_priority": [
                    {
                        "id": 1,
                        "value": "low"
                    },
                    {
                        "id": 2,
                        "value": "medium"
                    },
                    {
                        "id": 3,
                        "value": "high"
                    }
                ],
                "all_status": [
                    {
                        "id": 1,
                        "value": "todo"
                    },
                    {
                        "id": 2,
                        "value": "in-progress"
                    },
                    {
                        "id": 3,
                        "value": "completed"
                    },
                    {
                        "id": 4,
                        "value": "cancelled"
                    }
                ]
            }
        }
        ```


## Installation

* Please take clone from `main`

* First create your environment

```bash
    python -m venv env
```

* Activate your environment

    windows

```bash
    venv\Scripts\activate
```
    ubuntu

```bash
    source venv/bin/activate
```

* Install requirements (cd to requirements.txt folder) then

```bash
    pip install -r requirements.txt
```

* run migration

```bash
    python manage.py migrate
```

* createsuperuser of django admin panel (optional)
```bash
    python manage.py createsuperuser
```

* This project required `rabbitMQ` and `redis` for celery task

    for installing rabbitMQ follow below commands
    ```
        sudo apt-get install rabbitmq-server

        sudo service rabbitmq-server restart

        sudo rabbitmqctl status
    ```

    for installing redis follow below commands
    ```
        sudo apt install redis-server
    ```
    To verify our Redis installation, type the `redis-cli` command, then type ping then it should return pong

* After the installation of `rabbitMQ` and `redis` run below commands in two seperate terminals
    ```
        celery -A core worker --loglevel=info

        celery -A core flower
    ```


* Now installation steps are completed. For unit test we are using django's `TestCase & APITestCase` classes,
    To run unit test follow below command

    To run entire tests
    ```bash
        python manage.py test
    ```
    To run app wise

    ```bash
        python manage.py test user
        python manage.py test todo
    ```

* To run the project 

    ```bash
        python manage.py runserver
    ```

* settings.py
    ```
    # celery setup
    CELERY_TIMEZONE = TIME_ZONE
    CELERY_BROKER_URL = 'amqp://localhost'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379'
    CELERY_ACCEPT_CONTENT = ['application/json']
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_TASK_SERIALIZER = 'json'

    ```

`* note : if sending email is not working please change the EMAIL_HOST_USER and EMAIL_HOST_PASSWORD credentials in core\settings.py`

