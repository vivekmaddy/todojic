from django.contrib import admin
from .models import UserOTP
# Register your models here.

@admin.register(UserOTP)
class AdminUserOTP(admin.ModelAdmin):
    list_display = ('email', 'otp')
