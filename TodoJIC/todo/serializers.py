from rest_framework import serializers

from django.contrib.auth.models import User

from .models import TodoLists
from user.serializers import UserDetailsSerializer


class TodoListCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoLists
        fields = "__all__"

class TodoListsSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    priority = serializers.SerializerMethodField()

    class Meta:
        model = TodoLists
        fields = "__all__"

    def get_status(self, obj):
        return obj.get_status_display()

    def get_priority(self, obj):
        return obj.get_priority_display()
